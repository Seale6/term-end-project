/** Copyright 2020
 * @时间: 2020/10/3
 * @作者: 臧锡洋(Seale)
 * @blog: https://www.imsle.com
 * 请注明作者
 */

/**
 * 轮播图控件 -> base 基础版
 * @时间: 2020年10月4日
 * @版本: V1.0
 */
let end_carousel = (function () {
    // 插件运行进本信息
    let info = {
        // 绑定的dom
        carousel_dom: '',
        // 轮播图的总数
        carousel_total: 0,
        // 当前轮播图的指针索引
        carousel_currentIndex: 0,
        // pre Index
        carousel_preIndex: 0,
        // next Index
        carousel_nextIndex: 0,
        // 宽度和长度
        carousel_width: '0',
        carousel_height: '0',
    }
    let defaultSetting = {
        //TODO V1.2
        carousel_auto_play: false,
        carousel_delayTime: 0
    }

    /*****************暴露的接口**********************/
    /**
     * 初始化
     */
    let init = function (name, options) {
        info.carousel_dom = document.querySelector(name)
        if (options !== undefined) {
            defaultSetting.carousel_delayTime = options.carousel_delayTime
        }
        // 获取高度和宽度
        getHW4Carousel()
        // 获得total
        info.carousel_total = info.carousel_dom.children.length + 1
        // 初始化index
        info.carousel_preIndex = -1
        info.carousel_nextIndex = 1
        console.log(info)

        // 对carousel进行计算
        calculateInitLeft()
    }
    /**
     * 切换上一张轮播图
     */
    let pre_carousel = function () {
        calculateDomTranslate('pre')
    }
    /**
     * 切换下一张轮播图
     */

    let next_carousel = function () {
        calculateDomTranslate('next')
    }

    /**
     * 获得轮播图的总数
     */
    let total = function () {
        return info.carousel_total
    }

    /**
     * 获取当前索引位置
     * @returns {number} 返回的索引
     */
    let current = function () {
        return info.carousel_currentIndex
    }
    /*********************end**********************/

    /*****************inner method******************/
    // TODO hook函数
    /**
     * 获取当前绑定对象的高度和宽度
     */
    let getHW4Carousel = function () {
        if (!isDom()) return;
        let contentContainer = info.carousel_dom.children[2]
        // 获取当前内容容器的高度和宽度并存入info中
        info.carousel_height = contentContainer.clientHeight || contentContainer.offsetHeight
        info.carousel_width = contentContainer.clientWidth || contentContainer.offsetWidth
    }

    /**
     * 取得当前节点
     * @returns {Element}
     */
    let getCurrentDom = function () {
        return info.carousel_dom.children[2].children[info.carousel_currentIndex]
    }
    /**
     * 获得上一个carousel节点，如果当前节点为首节点，那么则返回-1
     * @returns {Element|number}
     */
    let getPreDom = function () {
        if (info.carousel_currentIndex === 0) return -1
        else return info.carousel_dom.children[2].children[info.carousel_currentIndex - 1]
    }
    /**
     * 获得下一个carousel节点，如果当前节点为最终节点，那么则返回-1
     * @returns {Element|number}
     */
    let getNextDom = function () {
        let maxIndex = info.carousel_dom.children.length - 1
        if (current() === maxIndex) return -1
        else return info.carousel_dom.children[2].children[info.carousel_currentIndex + 1]
    }

    /**
     * 获得当前carousel item节点
     * @returns {HTMLCollection}
     */
    let getItems = function () {
        return info.carousel_dom.children[2].children
    }
    /**
     * 计算节点移动
     * @param operation ->
     *  pre：往前移动
     *  next：往后移动
     *
     */
    let calculateDomTranslate = function (operation) {
        //我们需要将当前节点向前/向后滑出
        if (operation === 'pre') {
            console.log("pre")
            // 当前节点无前置节点
            if (info.carousel_currentIndex === 0) return;
            // 首先获取当前位置
            getCurrentDom().style.left = `calc(${getCurrentDom().style.left} + ${info.carousel_width}px)`
            // 指针后置
            info.carousel_currentIndex--;
            getCurrentDom().style.left = `calc(${getCurrentDom().style.left} + ${info.carousel_width}px)`


        } else if (operation === 'next') {
            // 当前节点无后置节点
            if (info.carousel_currentIndex === info.carousel_total - 1) return;
            // 首先获取当前位置
            getCurrentDom().style.left = `calc(${getCurrentDom().style.left} - ${info.carousel_width}px)`
            // 指针后置
            info.carousel_currentIndex++;
            getCurrentDom().style.left = `calc(${getCurrentDom().style.left} - ${info.carousel_width}px)`
        }
    }
    /**
     * 计算初始化时的left位置
     */
    let calculateInitLeft = function () {
        // 第一个默认不进行变化
        getItems()[0].style.left = "0"
        for (let i = 1; i < info.carousel_total; i++) {
            //从第二个item开始进行left操作
            getItems()[i].style.left = `${info.carousel_width}px`
        }
    }
    /**
     * 改变当前的carousel
     * @param toIndex 需要移动到的carousel item index
     */
    let changeCarousel = function (toIndex) {
        //TODO V1.1
    }

    /**
     * 判断是否已绑定dom，否则直接抛出异常
     * @returns {boolean|DOMException}
     */
    let isDom = function () {
        if (info.carousel_dom === undefined) {
            return new DOMException("NULL_DOM_EXCEPTION", "当前没有绑定Dom对象")
        }
        return true
    }

    /*暴露接口*/
    return {init, current, total, next_carousel, pre_carousel}
})()

/*Tab选项卡插件*/
let end_tab = (function () {
    // 插件运行信息
    let info = {
        tab_bom: '',
        tab_nav_title: '',
        tab_nav_desc: '',
        tab_nav_content: '',
        current_index: 1
    }

    /***************************************暴露的接口****************************************/
    /**
     * 初始化插件
     * @param name 插件的id
     */
    let init = function (name){
        // 绑定Dom
        info.tab_bom = document.querySelector(name)
        //初始化绑定
        getDom2Init()
        //初始化css
        initStyleStatus()
    }
    /**
     * 更改tab容器
     * @param index
     */
    let changeTab = function (index){
        changeStyleStatus(index)
    }

    /*********************************inner method***************************************/
    /**
     * 进行DOM初始化绑定
     */
    let getDom2Init = function (){
        info.tab_nav_title = info.tab_bom.children[0].children[0].children
        info.tab_nav_desc = info.tab_bom.children[0].children[1].children
        info.tab_nav_content = info.tab_bom.children[1].children
    }
    /**
     * 初始化CSS
     */
    let initStyleStatus = function (){
        if (!isDom()) return
        //init style
        // nav title
        if (!getNavTitle(info.current_index).classList.contains('active'))getNavTitle(info.current_index).classList.add('active')
        // desc
        getNavDesc(info.current_index).removeAttribute('style')
        // 遍历其他的desc容器,并且使其消失
        for (let i = 0 ; i < info.tab_nav_desc.length;i++){
            if (i === info.current_index) continue;
            getNavDesc(i).setAttribute('style','display:none')
        }
        // content
        getTabContent(info.current_index).removeAttribute('style')
        for (let i = 0 ; i < info.tab_nav_content.length; i++){
            if (i === info.current_index) continue;
            getTabContent(i).setAttribute('style','display:none')
        }
    }
    /**
     * 更换tab时执行改变Style操作
     * @param index 更改tab的索引
     */
    let changeStyleStatus = function (index){
        let now = info.current_index
        getNavTitle(now).classList.remove('active')
        getNavDesc(now).setAttribute('style','display:none')
        getTabContent(now).setAttribute('style','display:none')
        info.current_index = index
        initStyleStatus()
    }
    /**
     *  快速获取索引节点
     * @param index 当前索引
     * @returns {string}
     */
    let getNavTitle = function (index){
        return info.tab_nav_title[index]
    }
    let getNavDesc = function (index){
        return info.tab_nav_desc[index]
    }
    let getTabContent = function (index){
        return info.tab_nav_content[index]
    }
    /**
     * 判断是否已绑定dom，否则直接抛出异常
     * @returns {boolean|DOMException}
     */
    let isDom = function () {
        if (info.carousel_dom === undefined) {
            return new DOMException("NULL_DOM_EXCEPTION", "当前没有绑定Dom对象")
        }
        return true
    }
    return{init,changeTab}
})()